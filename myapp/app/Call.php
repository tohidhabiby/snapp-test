<?php

namespace App;

use App\Contracts\CallInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Call extends Model implements CallInterface
{
    const TABLE = 'calls';
    const ID = 'id';
    const TITLE = 'title';
    const PHONE_NUMBER = 'phone_number';
    const STATUS = 'status';
    const PENDING = 'pending';
    const ASSIGNED = 'assigned';
    const DONE = 'done';
    const IN_QUEUE = 'in_queue';
    const STATUS_OPTIONS = [self::PENDING, self::ASSIGNED, self::DONE, self::IN_QUEUE];


    /**
     * @return CallInterface|null
     */
    public static function getFirstCallInQueue(): ?CallInterface
    {
        return static::where(self::STATUS, self::IN_QUEUE)->orderBy(self::CREATED_AT)->first();
    }

    /**
     * @param User $user
     * @return CallInterface
     */
    public function assignedToUser(User $user): CallInterface
    {
        $this->users()->attach($user);
        $this->{self::STATUS} = self::ASSIGNED;
        $this->save();

        return $this;
    }

    /**
     * @return CallInterface
     */
    public function inQueue(): CallInterface
    {
        $this->{self::STATUS} = self::IN_QUEUE;
        $this->save();

        return $this;
    }

    /**
     * @return CallInterface
     */
    public function done(): CallInterface
    {
        $this->{self::STATUS} = self::DONE;
        $this->save();
        
        return $this;
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeWhereStatusIsAssigned(Builder $builder): Builder
    {
        return $builder->where(self::STATUS, self::ASSIGNED);
    }

    /**
     * @return BelongsToMany
     */
    public function users(): ?BelongsToMany
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * @param string $title
     * @param int $phoneNumber
     * @return CallInterface
     */
    public static function createFactory(string $title, int $phoneNumber): CallInterface
    {
        $call = new static();
        $call->{self::TITLE} = $title;
        $call->{self::PHONE_NUMBER} = $phoneNumber;
        $call->save();

        return $call;
    }

    /**
     * @param string $title
     * @param int $phoneNumber
     * @return CallInterface
     */
    public function updateFactory(string $title, int $phoneNumber): CallInterface
    {
        $this->update([self::TITLE => $title, self::PHONE_NUMBER => $phoneNumber]);

        return $this;
    }
}
