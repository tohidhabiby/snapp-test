<?php

namespace App\Contracts;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface CallInterface
{
    /**
     * @return null|BelongsToMany
     */
    public function users(): ?BelongsToMany;

    /**
     * @param string $title
     * @param int $phoneNumber
     * @return CallInterface
     */
    public static function createFactory(string $title, int $phoneNumber): CallInterface;

    /**
     * @param string $title
     * @param int $phoneNumber
     * @return CallInterface
     */
    public function updateFactory(string $title, int $phoneNumber): CallInterface;

    /**
     * @return CallInterface|null
     */
    public static function getFirstCallInQueue(): ?CallInterface;

    /**
     * @param User $user
     * @return CallInterface
     */
    public function assignedToUser(User $user): CallInterface;

    /**
     * @return CallInterface
     */
    public function inQueue(): CallInterface;

    /**
     * @return CallInterface
     */
    public function done(): CallInterface;

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeWhereStatusIsAssigned(Builder $builder): Builder;
}
