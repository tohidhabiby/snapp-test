<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface RoleInterface
{
    /**
     * @param string $title
     * @param string $priority
     * @return RoleInterface
     */
    public static function createFactory(string $title, string $priority): RoleInterface;

    /**
     * @param string $title
     * @param string $priority
     * @return RoleInterface
     */
    public function updateFactory(string $title, string $priority): RoleInterface;

    /**
     * @return null|BelongsToMany
     */
    public function users(): ?BelongsToMany;
}
