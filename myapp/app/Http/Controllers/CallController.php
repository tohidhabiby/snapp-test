<?php

namespace App\Http\Controllers;

use App\Call;
use App\Http\Requests\CallRequest;
use App\Http\Resources\CallResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return CallResource::collection(Call::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CallRequest $request
     * @return CallResource
     */
    public function store(CallRequest $request): CallResource
    {
        return new CallResource(Call::createFactory($request->get('title'), $request->get('phone_number')));
    }

    /**
     * Display the specified resource.
     *
     * @param Call $call
     * @return CallResource
     */
    public function show(Call $call): CallResource
    {
        return new CallResource($call);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CallRequest $request
     * @param Call $call
     * @return CallResource
     */
    public function update(CallRequest $request, Call $call): CallResource
    {
        return new CallResource($call->updateFactory($request->get('title'), $request->get('phone_number')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Call $call
     * @return CallResource|JsonResponse
     */
    public function destroy(Call $call)
    {
        try {
            $call->delete();
        } catch (Exception $exception) {
            return response()->json([], Response::HTTP_CONFLICT);
        }
        return new CallResource($call);
    }

    /**
     * @param Call $call
     * @return CallResource
     */
    public function done(Call $call): CallResource
    {
        return new CallResource($call->done());
    }
}
