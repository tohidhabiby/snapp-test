<?php

namespace App\Jobs;

use App\Call;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CallAssignerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Call $call */
    public $call;

    /**
     * Create a new job instance.
     *
     * @param Call $call
     */
    public function __construct(Call $call)
    {
        $this->call = $call;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $firstFreeUser = User::getFirstFreeUser();
        if ($firstFreeUser) {
            $this->call->assignedToUser($firstFreeUser);
        } else {
            $this->call->inQueue();
        }
    }
}
