<?php

namespace App\Jobs;

use App\Call;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckCallIsInQueueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Call */
    public $call;

    /**
     * Create a new job instance.
     *
     * @param Call $call
     */
    public function __construct(Call $call)
    {
        $this->call = $call;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $firstCallInQueue = Call::getFirstCallInQueue();

        if ($firstCallInQueue) {
            CallAssignerJob::dispatch($firstCallInQueue);
        }
    }
}
