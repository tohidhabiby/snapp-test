<?php

namespace App\Listeners;

use App\Events\CallCreated;
use App\Events\CallIsDone;
use App\Jobs\CallAssignerJob;
use App\Jobs\CheckCallIsInQueueJob;

class CallEventSubscriber
{
    /**
     * @param CallCreated $event
     */
    public function onCreate(CallCreated $event)
    {
        CallAssignerJob::dispatch($event->call);
    }

    /**
     * @param CallIsDone $event
     */
    public function onDone(CallIsDone $event)
    {
        CheckCallIsInQueueJob::dispatch($event->call);
    }

    /**
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(CallCreated::class, __CLASS__ . '@onCreate');
        $events->listen(CallIsDone::class, __CLASS__ . '@onDone');
    }
}
