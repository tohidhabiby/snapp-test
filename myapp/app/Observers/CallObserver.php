<?php

namespace App\Observers;

use App\Call;
use App\Events\CallCreated;
use App\Events\CallIsDone;

class CallObserver
{
    /**
     * Handle the call "created" event.
     *
     * @param  Call  $call
     * @return void
     */
    public function created(Call $call)
    {
        event(new CallCreated($call));
    }

    /**
     * Handle the call "updated" event.
     *
     * @param  \App\Call  $call
     * @return void
     */
    public function updated(Call $call)
    {
        if ($call->isDirty(Call::STATUS) && $call->{Call::STATUS} == Call::DONE) {
            event(new CallIsDone($call));
        }
    }

    /**
     * Handle the call "deleted" event.
     *
     * @param  \App\Call  $call
     * @return void
     */
    public function deleted(Call $call)
    {
        //
    }

    /**
     * Handle the call "restored" event.
     *
     * @param  \App\Call  $call
     * @return void
     */
    public function restored(Call $call)
    {
        //
    }

    /**
     * Handle the call "force deleted" event.
     *
     * @param  \App\Call  $call
     * @return void
     */
    public function forceDeleted(Call $call)
    {
        //
    }
}
