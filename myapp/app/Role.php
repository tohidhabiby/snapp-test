<?php

namespace App;

use App\Contracts\RoleInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model implements RoleInterface
{
    const TABLE = 'roles';
    const ID = 'id';
    const TITLE = 'title';
    const PRIORITY = 'priority';
    const LOW = 'low';
    const MEDIUM = 'medium';
    const HIGH = 'high';
    const PRIORITY_OPTIONS = [self::LOW, self::MEDIUM, self::HIGH];

    public $timestamps = false;

    protected $fillable = [self::PRIORITY, self::TITLE];

    /**
     * @param string $title
     * @param string $priority
     * @return RoleInterface
     */
    public static function createFactory(string $title, string $priority): RoleInterface
    {
        $role = new static();
        $role->{self::TITLE} = $title;
        $role->{self::PRIORITY} = $priority;
        $role->save();

        return $role;
    }

    /**
     * @param string $title
     * @param string $priority
     * @return RoleInterface
     */
    public function updateFactory(string $title, string $priority): RoleInterface
    {
        $this->update([self::TITLE => $title, self::PRIORITY]);

        return $this;
    }

    /**
     * @return null|BelongsToMany
     */
    public function users(): ?BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
