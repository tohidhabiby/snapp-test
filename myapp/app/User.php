<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;

    const TABLE = 'users';
    const ID = 'id';
    const USERNAME = 'username';
    const PASSWORD = 'password';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [self::USERNAME, self::PASSWORD];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [self::PASSWORD];

    /**
     * @param string $username
     * @param string $password
     * @return User
     */
    public static function createFactory(string $username, string $password): User
    {
        $user = new static();
        $user->{self::USERNAME} = $username;
        $user->{self::PASSWORD} = $password;
        $user->save();

        return $user;
    }

    /**
     * @param string $username
     * @param string $password
     * @return User
     */
    public function updateFactory(string $username, string $password): User
    {
        $this->update([self::USERNAME => $username, self::PASSWORD => $password]);

        return $this;
    }

    /**
     * @return null|BelongsToMany
     */
    public function roles(): ?BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @return BelongsToMany|null
     */
    public function calls(): ?BelongsToMany
    {
        return $this->belongsToMany(Call::class)->withTimestamps();
    }

    /**
     * @return Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public static function getFirstFreeUser()
    {
        $text = '';
        foreach (Role::PRIORITY_OPTIONS as $key => $value) {
            $text .= ' WHEN "' . $value . '" THEN ' . $key;
        }

        return User::query()
            ->select(['users.*', 'roles.priority'])
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->leftJoin('call_user', 'users.id', '=', 'call_user.user_id')
            ->leftJoin('calls', 'call_user.call_id', '=', 'calls.id')
            ->whereIn('users.id', User::query()->whereDoesntHave('calls', function ($join) {
                return $join->where(Call::STATUS, Call::ASSIGNED);
            })->pluck('users.id'))
            ->orderByRaw(
                "CASE priority " .
                $text .
                " END, priority"
            )
            ->first();
    }
}
