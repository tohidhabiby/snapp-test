<?php

/** @var Factory $factory */

use App\Call;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Call::class, function (Faker $faker) {
    return [
        Call::TITLE => $faker->word,
        Call::PHONE_NUMBER => $faker->numberBetween(10000, 10000000),
        Call::STATUS => $faker->randomElement(Call::STATUS_OPTIONS)
    ];
});

$factory->state(Call::class, Call::PENDING, function (Faker $faker) {
    return [
        Call::STATUS => Call::PENDING
    ];
});

$factory->state(Call::class, Call::DONE, function (Faker $faker) {
    return [
        Call::STATUS => Call::DONE
    ];
});

$factory->state(Call::class, Call::IN_QUEUE, function (Faker $faker) {
    return [
        Call::STATUS => Call::IN_QUEUE
    ];
});

$factory->state(Call::class, Call::ASSIGNED, function (Faker $faker) {
    return [
        Call::STATUS => Call::ASSIGNED
    ];
});
