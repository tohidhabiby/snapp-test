<?php

/** @var Factory $factory */

use App\Role;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Role::class, function (Faker $faker) {
    return [
        Role::TITLE => $faker->unique()->word,
        Role::PRIORITY => $faker->randomElement(Role::PRIORITY_OPTIONS)
    ];
});

    $factory->state(Role::class, Role::LOW, function (Faker $faker) {
    return [
        Role::PRIORITY => Role::LOW
    ];
});

$factory->state(Role::class, Role::MEDIUM, function (Faker $faker) {
    return [
        Role::PRIORITY => Role::MEDIUM
    ];
});

$factory->state(Role::class, Role::HIGH, function (Faker $faker) {
    return [
        Role::PRIORITY => Role::HIGH
    ];
});
