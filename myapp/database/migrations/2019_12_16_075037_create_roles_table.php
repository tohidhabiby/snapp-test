<?php

use App\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Role::TABLE, function (Blueprint $table) {
            $table->bigIncrements(Role::ID);
            $table->string(Role::TITLE)->unique();
            $table->enum(Role::PRIORITY, Role::PRIORITY_OPTIONS)
                ->comment(implode(', ', Role::PRIORITY_OPTIONS));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Role::TABLE);
    }
}
