<?php

use App\Call;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Call::TABLE, function (Blueprint $table) {
            $table->bigIncrements(Call::ID);
            $table->string(Call::TITLE);
            $table->bigInteger(Call::PHONE_NUMBER);
            $table->enum(Call::STATUS, Call::STATUS_OPTIONS)
                ->comment(implode(', ', Call::STATUS_OPTIONS))
                ->default(Call::PENDING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Call::TABLE);
    }
}
