<?php

use App\Call;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_user', function (Blueprint $table) {
            $table->unsignedBigInteger('call_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('call_id')->references(Call::ID)->on(Call::TABLE);
            $table->foreign('user_id')->references(User::ID)->on(User::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_user');
    }
}
