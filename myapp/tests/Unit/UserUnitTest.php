<?php

namespace Tests\Unit;

use App\Call;
use App\Role;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test  */
    public function itShouldReturnFreeUsersQuery()
    {
        $firstUser = factory(User::class)->create();
        dd($firstUser);
        $firstRole = factory(Role::class)->create();
        $firstUser->roles()->attach($firstRole);
        dd($firstUser->load('roles'));
    }
}
